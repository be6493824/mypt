# trường hợp với email:
1. không có email
<p><img src="/cap/cap1.png" width="600" height="600"></p>
2. email = null
<p><img src="/cap/cap2.png" width="600" height="600"></p>
3. email rỗng
<p><img src="/cap/cap3.png" width="600" height="600"></p>
4. không có email đó trong db
<p><img src="/cap/cap4.png" width="600" height="600"></p>

# trường hợp với permission_codes
1. không có permission_codes
<p><img src="/cap/cap5.png" width="600" height="600"></p>
2. permission_codes = null
<p><img src="/cap/cap6.png" width="600" height="600"></p>
3. permission_codes rỗng
<p><img src="/cap/cap7.png" width="600" height="600"></p>
4. chỉ có 1 permission code
<p><img src="/cap/cap8.png" width="600" height="600"></p>
5. từ 2 code trở lên là list
<p><img src="/cap/cap9.png" width="600" height="600"></p>

# trường hợp removeAll
1. Nếu removeAll được gọi và đặt là true -> xóa tất cả các quyền của user đó.
<p><img src="/cap/cap10.png" width="600" height="600"></p>
2. Nếu removeAll không được gọi, và có thể là None hoặc "" 
-> chỉ xóa theo permission_codes
<p><img src="/cap/cap11.png" width="600" height="600"></p>


### NOTE GIT

git remote add origin 'https://gitlab.com/be6493824/be.git'

// delete branch locally
git branch -d localBranchName

// delete branch remotely
git push origin --delete remoteBranchName