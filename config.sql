DROP DATABASE IF EXISTS `mypt_notification_stag_db`;
CREATE DATABASE IF NOT EXISTS `mypt_notification_stag_db` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `mypt_notification_stag_db`;

DROP DATABASE IF EXISTS `mypt_configs_stag_db`;
CREATE DATABASE IF NOT EXISTS `mypt_notification_stag_db` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `mypt_notification_stag_db`;

-- Dumping structure for table mypt_notification_stag_db.mypt_notification_configs
DROP TABLE IF EXISTS `mypt_managers_configs`;
CREATE TABLE IF NOT EXISTS `mypt_managers_configs` (
  `config_id` int(10) NOT NULL AUTO_INCREMENT,
  `config_type` enum('constant','message','remote') NOT NULL,
  `config_key` varchar(100) NOT NULL,
  `config_value` text DEFAULT NULL,
  `config_group` int(10) NOT NULL,
  `config_description_vi` text DEFAULT NULL,
  `config_description_en` text DEFAULT NULL,
  `config_status` enum('disabled','enabled','deleted') NOT NULL DEFAULT 'enabled',
  `owner` varchar(100) NOT NULL COMMENT 'email người insert',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `update_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `note` varchar(255) NOT NULL COMMENT 'dien giai dong config nay de lam gi',
  PRIMARY KEY (`config_id`) USING BTREE,
  UNIQUE KEY `config_key` (`config_key`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `mypt_managers_group_configs`;
CREATE TABLE IF NOT EXISTS `mypt_managers_group_configs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `group_title` varchar(100) NOT NULL,
  `group_keys` text DEFAULT NULL,
  `group_description` text DEFAULT NULL,
  `group_status` enum('disabled','enabled','deleted') NOT NULL DEFAULT 'enabled',
  `email_created` varchar(100) NOT NULL COMMENT 'email người insert',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `update_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `note` varchar(255) NOT NULL COMMENT 'dien giai dong config nay de lam gi',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;