from django.urls import path
from .my_http.views.health_view import *
from .my_http.views.login_view import *
from .my_http.views.token_view import *
from .my_http.views.permission_view import *
from .my_http.views.permission_tools_view import *
from .my_http.views.user_infos_view import *
from .my_http.views.token_mypt_view import *
from .my_http.views.auto_permission_view import *

urlpatterns = [
    path('health', HealthView.as_view({'get': 'health'})),
    path('show-login', LoginView.as_view({'get': 'showLogin'})),
    path('azure-login', LoginView.as_view({'get': 'showAzureLogin'})),
    path('gen-user-token-by-code', LoginView.as_view({'post': 'genUserTokenByCode'})),
    path('gen-user-token-by-azure-code', LoginView.as_view({'post': 'genUserTokenByAzureCode'})),
    path('user-token', TokenView.as_view({'post': 'genUserToken'})),
    path('logout', LoginView.as_view({'post': 'doLogout'})),
    path('gen-logginned-user-token', LoginView.as_view({'post': 'genLogginedUserToken'})),
    path('test-permission', LoginView.as_view({'post': 'testPermission'})),
    path('get-all-permissions', HoPermissionView.as_view({'post': 'getAllPermissions'})),
    path('save-permission-route-to-redis', HoPermissionView.as_view({'post': 'savePermissionWithRouteToRedis'})),
    path('get-permission-route-from-redis', HoPermissionView.as_view({'post': 'getPermissionWithRouteFromRedis'})),
    path('test-get-user-session', LoginView.as_view({'post': 'testGetUserSession'})),

    path('get-all-permissions-ho', HoPermissionToolsView.as_view({'post': 'getAllPermissionsHO'})),
    path('get-all-permission-groups-ho', HoPermissionToolsView.as_view({'post': 'getAllPermissionGroupsHO'})),
    path('get-all-permissions-by-group-code-ho',
         HoPermissionToolsView.as_view({'post': 'getPermissionsByGroupCodeHO'})),
    path('get-logged-in-user-permissions-ho', HoPermissionToolsView.as_view({'post': 'getLoggedInUserPermissionsHO'})),
    path('get-allowed-assign-permissions-web-ho',
         HoPermissionToolsView.as_view({'post': 'getAllowedAssignPermissionsWebHo'})),
    path('add-user-permissions-by-email-ho', HoPermissionToolsView.as_view({'post': 'addUserPermissionsByEmailHO'})),
    path('add-user-permissions-by-email-mypt',
         HoPermissionToolsView.as_view({'post': 'addUserPermissionsByEmailAppMyPT'})),
    path('update-one-child-depart-user-permission-ho',
         HoPermissionToolsView.as_view({'post': 'updateOneChildDepartUserPermissionHO'})),
    path('delete-one-user-permission-ho', HoPermissionToolsView.as_view({'post': 'deleteOneUserPermissionHO'})),

    path('add-mypt-permissions-to-tech-emp', HoPermissionToolsView.as_view({'post': 'addMyPTPermissionsToTechEmp'})),

    path('add-ho-pers-and-fea-roles-for-emails',
         HoPermissionToolsView.as_view({'post': 'addHoPersAndFeaRolesForEmails'})),

    path('save-employee', UserInfosView.as_view({'post': 'saveEmployee'})),
    path('add-all-permissions-web-ho', UserInfosView.as_view({'post': 'addAllPermissionsWebHO'})),
    path('add-all-permissions-app-mypt', UserInfosView.as_view({'post': 'addAllPermissionsAppMyPT'})),
    path('get-user-id-by-email', UserInfosView.as_view({'post': 'get_user_id_by_email'})),

    # authen bên thứ 3
    path('internal-api/gen-user-token', TokenMyptView.as_view({'post': 'genInternalApiUserToken'})),
    path('internal-api/decode-token', TokenMyptView.as_view({'post': 'decodeToken'})),
    path('internal-api/save-password', TokenMyptView.as_view({'post': 'save_password'})),
    path('internal-api/get-token-to-redis', TokenMyptView.as_view({'post': 'get_token_to_redis'})),

    # cập nhật email hàng loạt
    path('update-email-with-permissions-and-fea-roles', UserInfosView.as_view({'post': 'change_auth_user_info'})),

    # api test local
    path('test-api', UserInfosView.as_view({'post': 'test_api'})),

    path('all-ho-pers', HoPermissionToolsView.as_view({'get': 'getAllHOPers'})),
    path('add-pers-to-email', HoPermissionToolsView.as_view({'post': 'addPersToEmail'})),

    # tool
    path('get-redis-value-by-key', HoPermissionToolsView.as_view({'post': 'getRedisValueByKey'})),
    path('remove-redis-by-key', HoPermissionToolsView.as_view({'post': 'removeRedisByKey'})),

    # tool phan quyen
    path('auto-permission', AutoPermissionView.as_view({'post': 'api_auto_permission_for_web'})),
    path('list-detail-permission', AutoPermissionView.as_view({'post': 'api_list_detail_permission'})),
    path('save-list-permission', AutoPermissionView.as_view({'post': "api_save_list_permission"})),
    path('get-detail-permission-id-and-permission-group', AutoPermissionView.as_view({'post': 'api_get_detail_permission_id_and_permission_group'})),
    path('auto-update-permission-for-list-emp', AutoPermissionView.as_view({'post': 'api_auto_update_permission_for_list_emp'})),
    path('sync_permission_manual', AutoPermissionView.as_view({'post': 'api_auto_permssion_for_web_old'})),

    # Api sync email
    path('sync-email-employee', UserInfosView.as_view({'post': 'sync_email_employee'}), name='sync_email_employee'),

    # thuc hien logout cho 1 list cac email
    path('logout-manual-for-emails', LoginView.as_view({'post': 'logoutManualForEmails'})),
    
    # xoa quyen cua user qua email va permission_code
    path('remove-permission-by-emails', HoPermissionToolsView.as_view({'post': 'removePermissionByEmail'})),
]
