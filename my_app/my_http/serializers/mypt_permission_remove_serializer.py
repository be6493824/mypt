from rest_framework import serializers
from my_app.my_http.models.user_infos import UserInfos
from my_app.my_http.models.ho_permission import HoPermission
from my_app.my_http.models.ho_user_permission import HoUserPermission

class UserInfos_Serializer(serializers.ModelSerializer):
    class Meta:
        model = UserInfos
        fields = '__all__'
        
class HoPermission_Serializers(serializers.ModelSerializer):
    class Meta:
        model = HoPermission
        fields = '__all__'
        
# class HoUserPermission_Serializers(serializers.ModelSerializer):
#     class Meta:
#         model = HoUserPermission
#         fields = '__all__'