from rest_framework import serializers

from my_app.my_http.models.ho_user_permission import *
from rest_framework.serializers import ModelSerializer


class HoUserPermissionSerializer(ModelSerializer):
    userId = serializers.IntegerField(source='user_id')
    permissionId = serializers.IntegerField(source='permission_id')
    permissionCode = serializers.CharField(source='permission_code')
    childDepart = serializers.CharField(source='child_depart')
    dateCreated = serializers.DateTimeField(source='date_created')
    dateModified = serializers.DateTimeField(source='date_modified')
    updatedBy = serializers.IntegerField(source='updated_by')
    createdBy = serializers.IntegerField(source='created_by')

    class Meta:
        model = HoUserPermission
        fields = '__all__'


class InfoHoUserPermissionSerializer(ModelSerializer):
    userId = serializers.IntegerField(source='user_id', required=False)
    permissionId = serializers.IntegerField(source='permission_id', required=True, allow_null=False)
    permissionCode = serializers.CharField(source='permission_code', allow_null=True, allow_blank=True, required=True)
    childDepart = serializers.CharField(source='child_depart', allow_blank=True, allow_null=True, required=False)
    branch = serializers.CharField( allow_blank=True, allow_null=True, required=False)
    parentDepart = serializers.CharField( allow_blank=True, allow_null=True, required=False, source='parent_depart')
    agency = serializers.CharField( allow_blank=True, allow_null=True, required=False)
    dateCreated = serializers.DateTimeField(source='date_created', required=False, allow_null=True)
    dateModified = serializers.DateTimeField(source='date_modified', required=False, allow_null=True)
    updatedBy = serializers.IntegerField(source='updated_by', required=False, allow_null=True)
    createdBy = serializers.IntegerField(source='created_by', required=False, allow_null=True)
    assignMethod = serializers.CharField(source='assign_method', required=False, allow_null=True)

    class Meta:
        model = HoUserPermission
        fields = ['userId', 'permissionId', 'permissionCode', 'childDepart', 'dateCreated', 'dateModified', 'updatedBy',
                  'createdBy', 'assignMethod', 'branch', 'agency', 'parentDepart']


