from rest_framework import serializers
from my_app.my_http.models.ho_permission import *
from rest_framework.serializers import ModelSerializer


class HoPermissionSerializer(ModelSerializer):
    permissionId = serializers.IntegerField(source='permission_id')
    permissionName = serializers.CharField(source='permission_name')
    permissionCode = serializers.CharField(source='permission_code')
    permissionGroupId = serializers.IntegerField(source='permission_group_id')
    hasDepartRight = serializers.IntegerField(source='has_depart_right')
    dateDeleted = serializers.DateTimeField(source='date_deleted')
    dateCreated = serializers.DateTimeField(source='date_created')
    dateModified = serializers.DateTimeField(source='date_modified')
    isDeleted = serializers.IntegerField(source='is_deleted')
    updatedBy = serializers.IntegerField(source='updated_by')
    createdBy = serializers.IntegerField(source='created_by')

    class Meta:
        model = HoPermission
        fields = '__all__'

class InfoHoPermissionSerializer(ModelSerializer):
    permissionId = serializers.IntegerField(source='permission_id')
    permissionName = serializers.CharField(source='permission_name')
    permissionCode = serializers.CharField(source='permission_code')
    permissionGroupId = serializers.IntegerField(source='permission_group_id')
    hasDepartRight = serializers.IntegerField(source='has_depart_right')
    dateDeleted = serializers.DateTimeField(source='date_deleted')
    dateCreated = serializers.DateTimeField(source='date_created')
    dateModified = serializers.DateTimeField(source='date_modified')
    isDeleted = serializers.IntegerField(source='is_deleted')
    updatedBy = serializers.IntegerField(source='updated_by')
    createdBy = serializers.IntegerField(source='created_by')

    class Meta:
        model = HoPermission
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        exists = set(self.fields.keys())
        fields = set(kwargs.pop("fields", []) or exists)
        exclude = kwargs.pop("exclude", [])

        super().__init__(*args, **kwargs)

        for field in exclude + list(exists - fields):
            self.fields.pop(field, None)



