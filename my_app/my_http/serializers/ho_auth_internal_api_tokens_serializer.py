from rest_framework import serializers
from ...my_http.models.ho_auth_internal_api_tokens import *

# Create your models here.
class InternalApiTokensSerializer(serializers.ModelSerializer):
    token_id = serializers.CharField()
    caller_id = serializers.IntegerField()
    caller_user_name = serializers.CharField()
    expires_at = serializers.DateTimeField()
    revoked = serializers.IntegerField()
    date_created = serializers.DateTimeField(required=False, allow_null=True)
    date_modified = serializers.DateTimeField(required=False, allow_null=True)

    def __init__(self, *args, **kwargs):
        fields = kwargs.pop('fields', None)
        not_fields = kwargs.pop('not_fields', None)
        super().__init__(*args, **kwargs)
        if fields is not None:
            allowed = set(fields)
            existing = set(self.fields)
            for field_name in existing - allowed:
                self.fields.pop(field_name)
        if not_fields is not None:
            for item in not_fields:
                self.fields.pop(item)
    
    class Meta:
        model = InternalApiTokens
        fields = [
            'token_id',
            'caller_id',
            'caller_user_name',
            'expires_at',
            'revoked',
            'date_created',
            'date_modified'
        ]