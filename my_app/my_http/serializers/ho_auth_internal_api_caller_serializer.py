from rest_framework import serializers
from ...my_http.models.ho_auth_internal_api_caller import *

# Create your models here.
class InternalApiCallersSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    user_name = serializers.CharField()
    password = serializers.CharField()
    date_created = serializers.DateTimeField(required=False, allow_null=True)
    date_modified = serializers.DateTimeField(required=False, allow_null=True)

    def __init__(self, *args, **kwargs):
        fields = kwargs.pop('fields', None)
        not_fields = kwargs.pop('not_fields', None)
        super().__init__(*args, **kwargs)
        if fields is not None:
            allowed = set(fields)
            existing = set(self.fields)
            for field_name in existing - allowed:
                self.fields.pop(field_name)
        if not_fields is not None:
            for item in not_fields:
                self.fields.pop(item)
    
    class Meta:
        model = InternalApiCallers
        fields = [
            'id',
            'user_name',
            'password',
            'date_created',
            'date_modified'
        ]