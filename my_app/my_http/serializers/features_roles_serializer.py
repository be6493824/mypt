from rest_framework import serializers
from my_app.my_http.models.features_roles import FeaturesRoles
from rest_framework.serializers import ModelSerializer


class FeaturesRolesSerializer(ModelSerializer):

    class Meta:
        model = FeaturesRoles
        fields = '__all__'
