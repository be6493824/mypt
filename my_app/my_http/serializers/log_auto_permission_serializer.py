from rest_framework import serializers

from ..models.log_auto_permission import *


class LogPermissionSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False, allow_null=True)
    dataLog = serializers.CharField(source='data_log', allow_null=True, allow_blank=True, required=False)
    keyLog = serializers.CharField(source='key_log',  allow_null=True, allow_blank=True, required=False)
    createdBy = serializers.CharField(source='created_by',  allow_null=True, allow_blank=True, required=False)
    createdAt = serializers.DateTimeField(source='created_at', allow_null=True, required=False, format="%d/%m/%Y %H:%M:%S")

    class Meta:
        model = LogAutoPermission
        fields = ['id', 'dataLog', 'keyLog', 'createdBy', 'createdAt']

    def __init__(self, *args, **kwargs):
        exists = set(self.fields.keys())
        fields = set(kwargs.pop("fields", []) or exists)
        exclude = kwargs.pop("exclude", [])

        super().__init__(*args, **kwargs)

        for field in exclude + list(exists - fields):
            self.fields.pop(field, None)