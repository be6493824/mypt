from my_app.my_http.models.user_infos import UserInfos
from my_app.my_http.serializers.user_infos_serializer import UserInfosSerializer
from my_app.my_http.Entities.oauth_access_token_handler import OauthAccessTokenHandler
from my_app.my_http.Entities.oauth_refresh_token_handler import OauthRefreshTokenHandler
from my_app.my_core.Entities.centralized_session import CentralizedSession
from my_app.my_http.models.ho_oauth_access_token import HoOauthAccessToken
from my_app.my_http.serializers.ho_oauth_access_token_serializer import HoOauthAccessTokenSerializer
from my_app.my_http.models.ho_oauth_refresh_token import HoOauthRefreshToken
from my_app.my_http.models.ho_logout_logs import HoLogoutLogs
from datetime import datetime
import json

class AuthenHandler:
    def __init__(self):
        pass

    def doLogout(self, accessTokenJti):
        if accessTokenJti is None:
            return {
                "result": False,
                "errorCode": "ACCESS_TOKEN_JTI_NOT_FOUND"
            }
        accessTokenJti = str(accessTokenJti)

        # print("chuan bi tim user theo userId : " + accessTokenJti)
        # usQs = UserInfos.objects.filter(user_id=userId)[0:1]
        # userInfo_ser = UserInfosSerializer(usQs, many=True)
        # usersArr = userInfo_ser.data
        # if len(usersArr) <= 0:
        #     return {
        #         "result": False,
        #         "errorCode": "USER_NOT_FOUND"
        #     }
        # userInfo = usersArr[0]

        # Tim access token theo ID (accessTokenJti) de remove no
        print("chuan bi tim access token theo JTI : " + accessTokenJti)
        oatHandler = OauthAccessTokenHandler()
        oatRow = oatHandler.getAccessTokenById(accessTokenJti)
        if oatRow is None:
            return {
                "result": False,
                "errorCode": "ACCESS_TOKEN_NOT_FOUND"
            }

        # revoke access token nay
        resRevokeAccessToken = oatHandler.revokeAccessTokenById(accessTokenJti)
        # revoke refresh token
        ortHandler = OauthRefreshTokenHandler()
        resRevokeRefreshToken = ortHandler.revokeRefreshTokenById(oatRow.get("refresh_token_id"))

        # xoa key Access Token JTI trong Redis
        cenSessionObj = CentralizedSession()
        resRemoveRedisSession = cenSessionObj.removeSession(accessTokenJti)

        return {
            "result": True,
            "resRevokeAccessToken": resRevokeAccessToken,
            "resRevokeRefreshToken": resRevokeRefreshToken,
            "resRemoveRedisSession": resRemoveRedisSession
        }

    def doLogoutManualForUserIds(self, userIds = [], logoutContext = "MANUAL"):
        cenSessionObj = CentralizedSession()
        resultData = []
        for userId in userIds:
            userId = int(userId)
            if userId <= 0:
                continue
            # TODO: Xoa het cac Refresh Token va Access Token con thoi han cua userId nay
            # Lay ra cac Access Token con thoi han cua userId nay de chuan bi xoa session trong Redis
            oatQs = HoOauthAccessToken.objects.filter(user_id=userId, revoked=0)
            oat_ser = HoOauthAccessTokenSerializer(oatQs, many=True)
            accessTokensArr = oat_ser.data
            detailsData = {"countATExisting": len(accessTokensArr), "countATRevoked": 0, "countRTRevoked": 0}
            if len(accessTokensArr) > 0:
                # Xoa session cac access token cua userId nay trong Redis
                for accessTokenItem in accessTokensArr:
                    print("[doLogoutManualForUserIds - " + str(userId) + "] Xoa access token id : " + accessTokenItem["id"] + " trong Redis !")
                    resRemoveSession = cenSessionObj.removeSession(accessTokenItem["id"])
                # Update cot revoked cua cac Access Token con thoi han cua userId nay thanh 1
                print("[doLogoutManualForUserIds - " + str(userId) + "] Update revoked thanh 1 cho cac Access Token con thoi han cua userId  : " + str(userId))
                countAccessTokensRevoked = HoOauthAccessToken.objects.filter(user_id=userId, revoked=0).update(revoked=1, updated_at=datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
                detailsData["countATRevoked"] = countAccessTokensRevoked

            # Update cot revoked cua cac Refresh Token con thoi han cua userId nay thanh 1
            countRefreshTokensRevoked = HoOauthRefreshToken.objects.filter(user_id=userId, revoked=0).update(revoked=1, updated_at=datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            detailsData["countRTRevoked"] = countRefreshTokensRevoked

            # ghi log
            logoutLog = HoLogoutLogs()
            logoutLog.user_id = userId
            logoutLog.logout_context = logoutContext
            logoutLog.details_data = json.dumps(detailsData)
            logoutLog.save()

            resultData.append({
                "userId": userId,
                "detailsData": detailsData
            })

        return {
            "resultData": resultData
        }