from rest_framework.viewsets import ViewSet
from ...my_core.helpers.response import *
import uuid
import ast
import json
from my_app.configs import app_settings
from my_app.my_core.helpers import utils as utHelper
from datetime import datetime, timedelta
import redis
from my_app.my_http.models.ho_auth_internal_api_caller import *
from my_app.my_http.serializers.ho_auth_internal_api_tokens_serializer import *
from my_app.my_http.serializers.ho_auth_internal_api_caller_serializer import *
from django.conf import settings as project_settings

class TokenMyptView(ViewSet):
    redis_data = None
    key_token = "interapi:"
    expires_at_minutes = 24*60
    def __init__(self):
        self.redis_data = redis.StrictRedis(host=project_settings.REDIS_HOST_CENTRALIZED
            , port=project_settings.REDIS_PORT_CENTRALIZED
            , db=project_settings.REDIS_AUTH_API_CALLER_DATABASE_CENTRALIZED
            ,password=project_settings.REDIS_PASSWORD_CENTRALIZED
            , decode_responses=True, charset="utf-8")
        
    def create_new_token_to_redis(self, userIdStr, userNameStr):
        # TODO: lưu vào redis
        uuidStr = userIdStr + "_" + str(uuid.uuid4()) + "_" + str(int(datetime.now().timestamp()))
        redis_key = self.key_token + uuidStr
        encodedUserInfoStr = utHelper.encrypt_aes(app_settings.AES_SECRET_KEY, uuidStr)
        accessTokenStr = {
            "username": userNameStr,
            "accessToken": encodedUserInfoStr
        }
        accessTokenExpiredDt = datetime.now() + timedelta(minutes=self.expires_at_minutes)
        api_token_data = {
            "token_id": uuidStr,
            "caller_id": userIdStr,
            "caller_user_name": userNameStr,
            "expires_at": accessTokenExpiredDt.strftime("%Y-%m-%d %H:%M:%S"),
            "revoked": 0
        }
        serializer_token_data = InternalApiTokensSerializer(data=api_token_data)
        if not serializer_token_data.is_valid():
            return False, {
                "statusCode": 5,
                "message": "Không lưu được token",
                "data": {}
            }
        serializer_token_data.save()
        resSave = self.redis_data.set(redis_key, str(accessTokenStr), self.expires_at_minutes*60)
        if resSave == True:
            print("save centralized session thanh cong!")
        else:
            print("save centralized session that bai!")
        return True, accessTokenStr
        
    def genInternalApiUserToken(self, request):
        data = request.data.copy()
        if "userName" not in data or "passWord" not in data:
            return response_data(statusCode=5, message="Vui lòng nhập đầy đủ thông tin", data={})
        hash_password = utHelper.encrypt_aes(app_settings.AES_SECRET_KEY, data["passWord"])
        queryset = InternalApiCallers.objects.filter(user_name=data["userName"],password=hash_password)
        if not queryset.exists():
            return response_data(statusCode=6, message="sai username hoặc password", data={})
        userIdStr = str(queryset.values()[0]["id"])
        userNameStr = queryset.values()[0]["user_name"]
        queryset_api_token = InternalApiTokens.objects.filter(caller_user_name=userNameStr, revoked=0).order_by("-date_created").values()
        for item in queryset_api_token:
            encodedUserInfoStr = self.redis_data.get(self.key_token+item["token_id"])
            if encodedUserInfoStr is not None:
                # print("TOKEN CÒN HẠN")
                encodedUserInfoData = ast.literal_eval(encodedUserInfoStr)
                return response_data({
                    "username": encodedUserInfoData["username"],
                    "accessToken": encodedUserInfoData["accessToken"]
                })
            # print("TOKEN ĐÃ HẾT HẠN VÀ CHƯA UPDATE REVOKED THÀNH 1")
            queryset_update = InternalApiTokens.objects.get(token_id=item["token_id"])
            update_revoked = InternalApiTokensSerializer(queryset_update, data={"revoked": 1}, partial=True)
            if not update_revoked.is_valid():
                print("Update revoked không thành công")
            try:
                update_revoked.save()
                # print("Đã update được revoked")
            except:
                pass
        status, accessTokenStr = self.create_new_token_to_redis(userIdStr, userNameStr)
        # print("TẠO TOKEN MỚI THÀNH CÔNG")
        if not status:
            return response_data(
                statusCode=accessTokenStr["statusCode"],
                message=accessTokenStr["message"],
                data=accessTokenStr["data"]
            )
        return response_data(accessTokenStr)
    
    def decodeToken(self, request):
        data = request.data.copy()
        encodedUserTokenStr = data.get("Token")
        userTokenDataStr = utHelper.decrypt_aes(app_settings.AES_SECRET_KEY, encodedUserTokenStr)
        return response_data(userTokenDataStr)
    
    def save_password(self, request):
        data = request.data.copy()
        password = data.get("passWord")
        hash_password = utHelper.encrypt_aes(app_settings.AES_SECRET_KEY, password)
        data_save = InternalApiCallersSerializer(data={"user_name":data["userName"],"password":hash_password})
        if not data_save.is_valid():
            return response_data(statusCode=5, message=data_save.errors, data={})
        data_save.save()
        return response_data({})
        
    def get_token_to_redis(self, request):
        data = request.data.copy()
        username = data.get("userName")
        queryset = InternalApiTokens.objects.filter(caller_user_name=username,revoked=0).values()
        try:
            redis_value = self.redis_data.get(self.key_token+queryset[0]["token_id"])
            accessTokenData = ast.literal_eval(redis_value)
        except Exception as ex:
            return response_data(message=str(ex),data="")
        return response_data(accessTokenData)