from ...my_core.helpers.response import *
# import redis

from my_app.configs.response_codes import MESSAGE_API_ERROR_LOGIC, MESSAGE_API_NO_DATA, MESSAGE_API_SUCCESS, MESSAGE_API_NO_INPUT
from my_app.configs.response_codes import STATUS_CODE_ERROR_LOGIC, STATUS_CODE_NO_DATA, STATUS_CODE_SUCCESS, STATUS_CODE_INVALID_INPUT
from rest_framework.viewsets import ViewSet
from my_app.my_http.serializers.ho_permission_serializer import *
from my_app.my_http.serializers.ho_user_permission_serializer import *

# handler
from my_app.my_http.Entities.user_infos_handler import *
from my_app.my_http.Entities.permission_handler import PermissionHandler
from my_app.my_http.Entities.log_auto_permisison_handler import *
from my_app.my_http.Entities.authen_handler import AuthenHandler
from datetime import datetime

from django.db import connection

from my_app.my_core.helpers.utils import is_null_or_empty, get_str_datetime_now_import_db




class AutoPermissionView(ViewSet):
    def api_auto_permssion_for_web_old(self, request):

        # dùng để đồng bộ tay trong trường hợp có sai sót
        data_input = request.data.copy()

        try:
            save_log_class = LogAutoPermissionHandler()
            save_log_class.save_log_permission(str(data_input), "data_input", "")
            data_permission = data_input.get("data", [])
            assign_method_input = data_input.get('assign_method', 'auto_when_sync_hris')
            if len(data_permission) == 0:
                return response_data(data={}, message=MESSAGE_API_NO_INPUT, statusCode=STATUS_CODE_INVALID_INPUT)

            data_detail_permission_on_id = self.get_info_all_permission()

            # print(data_detail_permission_on_id)
            list_insert = []
            # print(data_permission)
            # print("==============")



            for i in data_permission:
                email_user = i['email']
                # userInfosHandlerObj = UserInfosHandler()
                user_info_handle_obj = UserInfosHandler()
                user_id = user_info_handle_obj.get_user_id_from_email(email_user) # sua lai cho nay
                action = i['actions']
                for i_permsion in action:
                    list_permission_id = i_permsion.get("permission_id", [])
                    action_type = i_permsion.get('action_type', "remove")
                    for k_permission_id in list_permission_id:
                        dict_data_permission = data_detail_permission_on_id.get(k_permission_id)
                        permission_code = dict_data_permission.get("permission_code", "")
                        right_on_child_depart = ""
                        if permission_code != "":
                            has_depart_right = dict_data_permission.get("has_depart_right", 0)


                            if has_depart_right == 1:
                                right_on_child_depart = i_permsion.get('right_on_child_depart', '')
                                if right_on_child_depart == "":
                                    continue

                        # print(user_id)
                        dict_save = {
                            "userId": user_id,
                            "permissionId": k_permission_id,
                            "permissionCode": permission_code,
                            "childDepart": right_on_child_depart,
                            'assignMethod': assign_method_input
                        }

                        # print(dict_save)

                        queryset = HoUserPermission.objects.filter(user_id=user_id, permission_id=k_permission_id,
                                                                   child_depart=right_on_child_depart)

                        if action_type == "remove":
                            if queryset.exists():
                                save_log_class_delete = LogAutoPermissionHandler()
                                save_log_class_delete.save_log_permission(str(queryset.values()), "data_delete", "")
                                queryset.delete()

                        else:
                            list_insert.append(dict_save)


                        # if action_type != "remove":
                        #     # neu khong bi trung data thi import, ko co update
                        #     if not queryset.exists():
                        #         serializer = InfoHoUserPermissionSerializer(data=dict_save)
                        #         if serializer.is_valid():
                        #             serializer.save()
                        #         else:
                        #             print("Error/loi: {}".format(serializer.errors))
                        # else:
                        #     if queryset.exists():
                        #         queryset.delete()


            list_err = []
            # print(list_insert)
            if len(list_insert) > 0 :
                for k_insert in list_insert:
                    class_permission = PermissionHandler()
                    list_err_tmp = class_permission.provide_permission(k_insert)
                    list_err.extend(list_err_tmp)



                    # user_id = k_insert['userId']
                    # permission_id = k_insert['permissionId']
                    # right_on_child_depart = k_insert['childDepart']
                    # queryset = HoUserPermission.objects.filter(user_id=user_id, permission_id=permission_id)
                    # if not queryset.exists():
                    #     # them moi value
                    #     serializer = InfoHoUserPermissionSerializer(data=k_insert)
                    #     if serializer.is_valid():
                    #         serializer.save()
                    #     else:
                    #         list_err.append(serializer.errors)
                    # else:
                    #     # update value
                    #     serializer = InfoHoUserPermissionSerializer(queryset.first(), data=k_insert)
                    #     if serializer.is_valid():
                    #         serializer.save()
                    #     else:
                    #         list_err.append(serializer.errors)

            return response_data(data=list_err, message=MESSAGE_API_SUCCESS, statusCode=STATUS_CODE_SUCCESS)

        except Exception as ex:
            print("api_auto_permssion_for_web >> Error/Loi: {}".format(ex))
            return response_data(data=str(ex), message=MESSAGE_API_ERROR_LOGIC, statusCode=STATUS_CODE_ERROR_LOGIC)

    def api_auto_permssion_for_web_v1(self, request):
        # api cu dang chay tren prod
        data_input = request.data.copy()

        try:
            save_log_class = LogAutoPermissionHandler()
            save_log_class.save_log_permission(str(data_input), "data_input", "")
            data_permission = data_input.get("data", [])
            assign_method_input = data_input.get('assign_method', 'auto_when_sync_hris')
            if len(data_permission) == 0:
                return response_data(data={}, message=MESSAGE_API_NO_INPUT, statusCode=STATUS_CODE_INVALID_INPUT)

            data_detail_permission_on_id = self.get_info_all_permission()

            list_insert = []



            for i in data_permission:
                email_user = i['email']
                parent_depart_user = i['parentDepart']
                # userInfosHandlerObj = UserInfosHandler()
                user_info_handle_obj = UserInfosHandler()
                user_id = user_info_handle_obj.get_user_id_from_email(email_user) # sua lai cho nay
                action = i['actions']
                for i_permsion in action:
                    list_permission_id = i_permsion.get("permission_id", [])
                    action_type = i_permsion.get('action_type', "remove")
                    for k_permission_id in list_permission_id:
                        dict_data_permission = data_detail_permission_on_id.get(k_permission_id)
                        permission_code = dict_data_permission.get("permission_code", "")
                        right_on_child_depart = ""
                        has_depart_right = dict_data_permission.get("has_depart_right", 0)


                        if has_depart_right == 1:
                            right_on_child_depart = i_permsion.get('right_on_child_depart', '')
                            if right_on_child_depart == "":
                                continue

                        dict_save = {
                            "userId": user_id,
                            "permissionId": k_permission_id,
                            "permissionCode": permission_code,
                            "childDepart": right_on_child_depart,
                            'assignMethod': assign_method_input
                        }




                        if action_type == "remove":
                            if parent_depart_user == "":
                                continue

                            class_permisison_handle = PermissionHandler()
                            class_permisison_handle.delete_permission_on_agency(right_on_child_depart, user_id,
                                                                                k_permission_id)
                        else:
                            dict_save.update({
                                'has_right_on_child_depart': has_depart_right
                            })
                            list_insert.append(dict_save)

            list_err = []
            if len(list_insert) > 0:
                for k_insert in list_insert:


                    class_permission = PermissionHandler()
                    list_err_tmp = class_permission.provide_permission_v2(k_insert)
                    list_err.extend(list_err_tmp)
                    if len(list_err) > 0:
                        save_log_class.save_log_permission(str(list_err), "error", "")


            return response_data(data=list_err, message=MESSAGE_API_SUCCESS, statusCode=STATUS_CODE_SUCCESS)

        except Exception as ex:
            print("api_auto_permssion_for_web >> Error/Loi: {}".format(ex))
            return response_data(data=str(ex), message=MESSAGE_API_ERROR_LOGIC, statusCode=STATUS_CODE_ERROR_LOGIC)

    # dung de chay code upgrade bang ho_user_permission
    def api_auto_permission_for_web(self, request):
        data_input = request.data.copy()



        try:
            save_log_class = LogAutoPermissionHandler()
            save_log_class.save_log_permission(str(data_input), "data_input", "")
            data_permission = data_input.get("data", [])
            assign_method_input = data_input.get('assign_method', 'auto_when_sync_hris')
            if len(data_permission) == 0:
                return response_data(data={}, message=MESSAGE_API_NO_INPUT, statusCode=STATUS_CODE_INVALID_INPUT)

            data_detail_permission_on_id = self.get_info_all_permission()

            list_insert = []
            list_user_id = []

            for i in data_permission:
                email_user = i['email']
                parent_depart_user = i['parentDepart']
                # userInfosHandlerObj = UserInfosHandler()
                user_info_handle_obj = UserInfosHandler()
                user_id = user_info_handle_obj.get_user_id_from_email(email_user)  # sua lai cho nay
                action = i['actions']
                for i_permsion in action:
                    list_permission_id = i_permsion.get("permission_id", [])
                    action_type = i_permsion.get('action_type', "remove")
                    for k_permission_id in list_permission_id:
                        dict_data_permission = data_detail_permission_on_id.get(k_permission_id)
                        permission_code = dict_data_permission.get("permission_code", "")
                        # right_on_child_depart = []
                        has_depart_right = dict_data_permission.get("has_depart_right", 0)
                        dict_save = self.get_dict_general_info_permission_save(user_id, k_permission_id, permission_code, assign_method_input)

                        if has_depart_right == 1:
                            right_on_child_depart = i_permsion.get('right_on_child_depart', [])
                            if right_on_child_depart == []:
                                continue

                            for i_depart in right_on_child_depart:
                                depart = i_depart['depart']
                                type_depart = i_depart['type_depart']
                                dict_apply_depart = self.get_dict_save_user_permission_applied_depart(depart, type_depart)
                                if len(dict_apply_depart) > 0 :
                                    dict_save.update(dict_apply_depart)


                        if action_type == "remove":
                            if parent_depart_user == "":
                                continue

                            class_permisison_handle = PermissionHandler()
                            class_permisison_handle.delete_permission_on_user(dict_save)
                            # class_permisison_handle.delete_permission_on_agency(right_on_child_depart, user_id,
                            #                                                     k_permission_id)
                        else:
                            dict_save.update({
                                'has_right_on_child_depart': has_depart_right
                            })
                            list_insert.append(dict_save)
                list_user_id.append(user_id)

            list_err = []
            # cap quyen

            if len(list_insert) > 0:
                for k_insert in list_insert:

                    class_permission = PermissionHandler()
                    list_err_tmp = class_permission.provide_permission_v3(k_insert)
                    list_err.extend(list_err_tmp)
                    if len(list_err) > 0:
                        save_log_class.save_log_permission(str(list_err), "error", "")

            # dang xuat khoi app
            class_auth = AuthenHandler()
            reason_logout = assign_method_input.upper()
            logout = class_auth.doLogoutManualForUserIds(list_user_id, reason_logout)




            return response_data(data=list_err, message=MESSAGE_API_SUCCESS, statusCode=STATUS_CODE_SUCCESS)

        except Exception as ex:
            print("api_auto_permssion_for_web >> Error/Loi: {}".format(ex))
            return response_data(data=str(ex), message=MESSAGE_API_ERROR_LOGIC, statusCode=STATUS_CODE_ERROR_LOGIC)

    def get_dict_general_info_permission_save(self,user_id, permission_id, permission_code, assign_method_input):
        dict_data = {
                        "userId": user_id,
                        "permissionId": permission_id,
                        "permissionCode": permission_code,
                        'assignMethod': assign_method_input
                     }
        return dict_data

    def get_dict_save_user_permission_applied_depart(self, applied_depart, type_depart):
        # dict_save = {
        #     "userId": user_id,
        #     "permissionId": permission_id,
        #     "permissionCode": permission_code,
        #     "childDepart": right_on_child_depart,
        #     'assignMethod': assign_method_input
        # }

        if type_depart == "branch":
            dict_save = {
                "branch": applied_depart,
            }
            return dict_save
        elif type_depart == "parent_depart":
            dict_save = {

                "parentDepart": applied_depart,

            }
            return dict_save
        elif type_depart == "agency":

            dict_save = {

                "agency": applied_depart,

            }
            return dict_save
        elif type_depart == "child_depart":
            dict_save = {

                "childDepart": applied_depart,

            }
            return dict_save

        else:
            return {}

    def api_save_list_permission(self, request):
        data_input = request.data
        try:

            save_log_class = LogAutoPermissionHandler()
            save_log_class.save_log_permission(str(data_input), "data_input", "")
            data_add = data_input.get("data_add", [])
            data_delete = data_input.get("data_delete", [])
            assign_method_input = data_input.get('assign_method', 'auto_when_edit_job_per_group')
            if len(data_add) == 0 and data_delete == 0:
                return response_data(data={}, message=MESSAGE_API_NO_INPUT, statusCode=STATUS_CODE_INVALID_INPUT)

            data_detail_permission_on_id = self.get_info_all_permission()
            list_err = []
            list_user_id = []

            if len(data_delete) > 0:
                list_err_delete, list_user_delete = self.process_list_data_auto_permission(data_delete, data_detail_permission_on_id, "delete")
                list_err.extend(list_err_delete)
                list_user_id.extend(list_user_delete)

            if len(data_add) > 0:
                list_err_add,list_user_add = self.process_list_data_auto_permission(data_add, data_detail_permission_on_id, "add", assign_method_input)
                list_err.extend(list_err_add)
                list_user_id.extend(list_user_add)


            # xu ly logout
            class_auth = AuthenHandler()
            reason_logout = assign_method_input.upper()
            logout = class_auth.doLogoutManualForUserIds(list_user_id, reason_logout)

            if len(list_err) > 0:
                return response_data(data=list_err, message="xử lý có lỗi ở một vài data", statusCode=STATUS_CODE_ERROR_LOGIC)
            return response_data(data={}, message=MESSAGE_API_SUCCESS, statusCode=STATUS_CODE_SUCCESS)






        except Exception as ex:
            return response_data(data=str(ex), message=MESSAGE_API_ERROR_LOGIC, statusCode=STATUS_CODE_ERROR_LOGIC)


    def process_list_data_auto_permission(self, list_data,data_detail_permission_on_id, _type="add", assign_method='auto_when_edit_job_per_group'):
        # phan quyen tu dong cho 1 list email, voi action la 1 list permision id
        list_err = []
        list_user_id = []

        for i in list_data:
            email_user = i['email']
            # userInfosHandlerObj = UserInfosHandler()
            user_info_handle_obj = UserInfosHandler()
            user_id = user_info_handle_obj.get_user_id_from_email(email_user)
            list_user_id.append(user_id)


            action = i['actions']  # action la list_permission_id
            if len(action) == 0:
                return list_err

            parent_depart = i['parentDepart']
            right_on_child_depart = i['rightOnChildDepart']



            if is_null_or_empty(parent_depart):
                right_on_child_depart = []

            dict_apply_depart = {}
            for i_depart in right_on_child_depart:
                depart = i_depart['depart']
                type_depart = i_depart['type_depart']
                dict_apply_depart_tmp = self.get_dict_save_user_permission_applied_depart(depart, type_depart)
                dict_apply_depart.update(dict_apply_depart_tmp)





            for i_permission in action:
                dict_data_permission = data_detail_permission_on_id.get(i_permission)
                permission_code = dict_data_permission.get("permission_code", "")

                has_right_on_child_depart = dict_data_permission.get("has_depart_right", "")
                dict_save = self.get_dict_general_info_permission_save(user_id, i_permission, permission_code, assign_method)



                # dict_save = {
                #     "userId": user_id,
                #     "permissionId": i_permission,
                #     "permissionCode": permission_code,
                #     "childDepart": right_on_child_depart,
                #     'assignMethod': assign_method
                # }
                if len(dict_apply_depart) > 0:
                    dict_save.update(dict_apply_depart)



                #todo cap quyen theo chi nhanh

                if _type == "delete":
                    if is_null_or_empty(parent_depart):
                        # neu chua xac dinh duoc phong ban thi khong xoa quyen
                        continue
                    class_permission_handle = PermissionHandler()
                    # print(dict_save)
                    class_permission_handle.delete_permission_on_user(dict_save)
                    # class_permission_handle.delete_permission_on_agency(right_on_child_depart, user_id,
                    #                                                     i_permission)

                        # save_log_permission_class = LogAutoPermissionHandler()
                        # save_log_permission_class.save_log_permission(str(queryset), "data_delete", "")
                        # queryset.delete()
                else:

                    if has_right_on_child_depart ==1:
                        if len(right_on_child_depart) == 0:
                            continue


                    class_permission = PermissionHandler()
                    dict_save.update({
                        'has_right_on_child_depart': has_right_on_child_depart
                    })
                    list_err_tmp = class_permission.provide_permission_v3(dict_save)
                    list_err.extend(list_err_tmp)

        return list_err, list_user_id

    def get_info_all_permission(self):
        dict_data = {}
        try:
            queryset = HoPermission.objects.filter(is_deleted=0).values('permission_id', 'permission_code', 'has_depart_right')
            if len(queryset) > 0:

                for i in queryset:
                    permission_id = i['permission_id']
                    permission_code = i['permission_code']
                    has_depart_right = i['has_depart_right']
                    dict_data.update({
                        permission_id:{
                            "permission_code": permission_code,
                            "has_depart_right": has_depart_right
                        }
                    })

        except Exception as ex:
            print("get_info_all_permission >> Error/loi: {}".format(ex))
        return dict_data

    def api_get_detail_permission_id_and_permission_group(self, request):
        # api lay thong tin  quyen
        dict_data_permission_id = {}
        try:
            # mypt_ho_auth_permission
            query = "SELECT a.permission_id, a.permission_name, a.permission_code,  b.permission_group_id, b.permission_group_name, a.updated_by, a.date_modified " \
                    "FROM mypt_ho_auth_permission a INNER JOIN mypt_ho_auth_permission_group b ON a.permission_group_id = b.permission_group_id"

            cursor = connection.cursor()
            cursor.execute(query)
            data_row = cursor.fetchall()

            if len(data_row) == 0 :
                return response_data(data={}, message="không có data", statusCode=0)
            for i in data_row:
                date_modify = i[6]
                str_date_modify = ""
                updated_by = i[5]
                if date_modify is not  None:
                    str_date_modify = datetime.strftime(date_modify, "%d/%m/%Y %H:%M:%S")
                dict_data_permission_id.update({
                    i[0]: {
                        'permission_name': i[1],
                        'permission_code': i[2],
                        'permission_group_id': i[3],
                        'permission_group_name': i[4],
                        "updatedAt": str_date_modify,
                        "updatedBy": updated_by
                    }
                })
            return response_data(data=dict_data_permission_id, message="Thành công", statusCode=1)

        except Exception as ex:
            return response_data(data=str(ex), message="Thất bại", statusCode=0)

    def api_list_detail_permission(self, request):
        # api lay tra ve danh sach cac quyen show tren web
        try:
            query = "SELECT a.permission_id, a.permission_name,  b.permission_group_name " \
                    "FROM mypt_ho_auth_permission a INNER JOIN mypt_ho_auth_permission_group b ON a.permission_group_id = b.permission_group_id WHERE b.permission_group_id NOT IN (1,16,19,20) "

            cursor = connection.cursor()
            cursor.execute(query)
            data_row = cursor.fetchall()
            if len(data_row) == 0:
                return response_data(data={}, message=MESSAGE_API_NO_INPUT, statusCode=STATUS_CODE_NO_DATA)
            list_data = []
            for i in data_row:
                dict_tmp = {
                    'Mã thao tác': i[0],
                    'Thao tác thực hiện': i[1],
                    'Tên chức năng': i[2]
                }
                list_data.append(dict_tmp)
            return response_data(data=list_data, message=MESSAGE_API_SUCCESS, statusCode=STATUS_CODE_SUCCESS)
        except Exception as ex:
            return response_data(data=str(ex), message=MESSAGE_API_ERROR_LOGIC, statusCode=STATUS_CODE_ERROR_LOGIC)




