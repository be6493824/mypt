from django.db import models

# Create your models here.
class InternalApiTokens(models.Model):
    class Meta:
        db_table = 'mypt_ho_auth_internal_api_tokens'

    token_id = models.CharField(primary_key=True, max_length=100)
    caller_id = models.IntegerField()
    caller_user_name = models.CharField(max_length=255)
    expires_at = models.DateTimeField()
    revoked = models.IntegerField(max_length=1, default=0)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
