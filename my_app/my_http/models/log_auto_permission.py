
from django.db import models


# Create your models here.
class LogAutoPermission(models.Model):
    class Meta:
        db_table = 'mypt_ho_auth_log_auto_permission'

    id = models.AutoField(primary_key=True)
    data_log = models.TextField()
    key_log = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.CharField(max_length=255)
