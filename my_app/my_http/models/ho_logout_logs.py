from django.db import models

# Create your models here.
class HoLogoutLogs(models.Model):
    class Meta:
        db_table = 'mypt_ho_auth_logout_logs'

    log_id = models.AutoField(primary_key=True)
    user_id = models.IntegerField()
    logout_context = models.CharField(max_length=100)
    details_data = models.CharField(max_length=1000)
    created_at = models.DateTimeField(auto_now_add=True)
