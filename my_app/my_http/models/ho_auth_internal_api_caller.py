from django.db import models

# Create your models here.
class InternalApiCallers(models.Model):
    class Meta:
        db_table = 'mypt_ho_auth_internal_api_callers'

    id = models.AutoField(primary_key=True)
    user_name = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
